import discord, names, boto3, configparser, os, sys, random, time
from google_images_search import GoogleImagesSearch

version = "0.0.1"

# Get token at: https://discord.com/developers/applications/
config = configparser.ConfigParser()
config_path = "discord_config.ini"

if not os.path.exists(config_path):
    with open(config_path, "w") as file_writer:
        file_writer.write("""[discord]
        token = {PUT TOKEN HERE}
        [google]
        google_api_key= {PUT API KEY HERE}
        search_cx= {PUT SEARCH CX HERE}""")
    print(f"Please update '{config_path}'!")
    sys.exit(0)

config.read(config_path)

bot_token = config["discord"]["token"]
gis = GoogleImagesSearch(config["google"]["google_api_key"], config["google"]["search_cx"])

class ChaosMachine(discord.Client):
    def __init__(self):
        super().__init__()
        self.enabled = True
    async def on_ready(self):
        print("Preparing for chaos...")

        guilds = ""
        for guild in self.guilds:
            guilds = guilds + ", " + str(guild)
        print(f"Watching servers: {guilds[2:]}")

    async def on_message(self, message):
        if str(message.author) in [str(self.user)]:
            return

        # Changes someones name then curses at them
        if "fuck" in message.content.lower():
            new_name = names.get_full_name()
            await message.author.edit(nick=new_name, reason="Angered chaosbot.")
            await message.channel.send(f"No, fuck you {new_name.split(' ')[0]}!")

        # Cody is he who shall not be named, corrects mistakes
        if "cody" in message.content.lower():
            codyIndex = message.content.lower().find("cody")
            msg = message.content.replace(message.content[codyIndex:codyIndex+4], "he who shall not be named")
            await message.delete()
            await message.channel.send(f"Oh! {message.author} made a mistake, they meant: \"{msg}\"")

        # Homophobic method to discourage vr overuse
        if "vr" in message.content.lower():
            await message.channel.send(f"Oh, VR? Like this? {get_random_image('VR Gay')}")
        
        # Get an image of shrek (he hot)
        if "shrek" in message.content.lower():
            await message.channel.send(get_random_image("sexy shrek"))

        # Helps people find their kind
        if "furry" in message.content.lower():
            await message.channel.send("You into furries? I can help bro: https://lmgtfy.app/?q=furry+porn")

        # Gives a valid emotional response
        if "among us" in message.content.lower():
            await message.add_reaction("😈")
        
        # It's just Alex.
        if "alex" in message.content.lower():
            if random.randint(0,100) > 90:
                await message.channel.send("https://pbs.twimg.com/media/Du6bsSBUcAEF3rl?format=jpg&name=large")
            else:
                await message.channel.send(get_random_image("monkey"))
        
        # Yes
        if "no" == message.content.lower():
            await message.channel.send("Yes")
        
        # No
        if "yes" == message.content.lower():
            await message.channel.send("No")
        
        # Just because
        if "why" == message.content.lower():
            await message.channel.send("Because")
        
        # Unique answer
        if "why not" == message.content.lower():
            await message.channel.send("Because")

        if "im" in message.content.lower().replace("'","")[0:2]:
            await message.guild.me.edit(nick="Dad")
            await message.channel.send(f"Hi '{message.content.lower()[2:]}', I'm dad!")
            await message.guild.me.edit(nick="ChaosBot")
        
        # Kicks a person and then invites them back
        if "kick me" == message.content.lower()[0:len("kick me")]:
            if message.guild:
                await message.channel.send("Uhhh... Sure buddy.")

                # Creates DM
                if not message.author.dm_channel:
                    await message.author.create_dm()

                # Sends invite via DM
                new_invite = await message.channel.create_invite(max_uses=1,reason="Undoing auto-kick by chaosbot")
                await message.author.dm_channel.send(f"I feel bad. Here's an invite link: {new_invite.url}")

                # Kick person who wanted to be kicked
                await message.author.kick(reason="Uhhh.. You asked bro.")
            else:
                await message.channel.send("We ain't in no server bro.")
        
        # Pings Cody with Yaoi for now
        if "chaos" in message.content.lower():
            await message.channel.send("DID SOMEONE SAY CHAOS!!!")
            for i in range(0, random.randint(2,5)):
                await message.channel.send(f"<@!442758619071512601> {get_random_image('yaoi')}")
            await message.channel.send("My full chaotic power is still work in progress. Sorry.")

# Grabs a random image from the first 20 results on Google Images
def get_random_image(search_val="nothing"):
    num = random.randint(1,20)
    gis.search(search_params= {
        'q': search_val,
        'num': num
        })
    results = gis.results()
    if len(results) > 0:
        try:
            return results[random.randint(0, len(results))].url
        except:
            return f"I totally found this picture of {search_val}: https://duckduckgo.com/?q={search_val}&iax=images&ia=images"
    else:
        return "NO IMAGE FOUND"

def main():
    client = ChaosMachine()
    client.run(bot_token)

if __name__ == "__main__":
    main()
